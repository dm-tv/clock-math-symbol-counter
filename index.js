function IndexComponent() {
  const sidebarItemId = {
    CLOCK: 'sidebarItemClockLi',
    MATH_FUNCTIONS: 'sidebarItemMathFunctionsLi',
    SYMBOL_COUNTER: 'sidebarItemSymbolCounterLi',
  };

  // available dom elements
  const sidebarItemClockLi = document.getElementById(sidebarItemId.CLOCK);
  const sidebarItemMathFunctionsLi = document.getElementById(sidebarItemId.MATH_FUNCTIONS);
  const sidebarItemSymbolCounterLi = document.getElementById(sidebarItemId.SYMBOL_COUNTER);
  const contentSectionIframe = document.getElementById('contentSection');

  const sidebarItemComponent =
    {
      [sidebarItemId.CLOCK]: {
        templatePath: './components/clock/clock.component.html'
      },
      [sidebarItemId.MATH_FUNCTIONS]: {
        templatePath: './components/math-functions/math-functions.component.html'
      },
      [sidebarItemId.SYMBOL_COUNTER]: {
        templatePath: './components/symbols-counter/symbols-counter.component.html'
      }
    };

  let activeSidebarItem;

  const initSubscriptions = () => {
    sidebarItemClockLi.addEventListener('click', () => {
      onSidebarMenuItemChanged(sidebarItemClockLi);
    });
    sidebarItemMathFunctionsLi.addEventListener('click', () => {
      onSidebarMenuItemChanged(sidebarItemMathFunctionsLi);
    });

    sidebarItemSymbolCounterLi.addEventListener('click', () => {
      onSidebarMenuItemChanged(sidebarItemSymbolCounterLi);
    })
  };

  const setDefaultValues = () => {
    selectSidebarItem(sidebarItemClockLi);
    renderChildComponent(sidebarItemClockLi);

    activeSidebarItem = sidebarItemClockLi;
  };

  const onSidebarMenuItemChanged = (selectedSidebarItem) => {

    if (activeSidebarItem !== selectedSidebarItem) {
      selectSidebarItem(selectedSidebarItem);
      renderChildComponent(selectedSidebarItem);

      activeSidebarItem = selectedSidebarItem;
    }
  };

  const selectSidebarItem = (selectedSidebarItem) => {
    const activeClassName = 'active';

    if (activeSidebarItem) {
      activeSidebarItem.classList.remove(activeClassName);
    }

    selectedSidebarItem.classList.add(activeClassName);
  };

  const renderChildComponent = (selectedSidebarItem) => {
    contentSectionIframe.src = sidebarItemComponent[selectedSidebarItem.id].templatePath;
  };

  this.initComponent = () => {
    setDefaultValues();

    initSubscriptions();
  }


}

