function SymbolsCounterComponent() {
  // Available dom elements
  const strInput = document.getElementById('strInput');
  const symbolsInput = document.getElementById('symbolsInput');
  const resultSpan = document.getElementById('resultSpan');
  const searchButton = document.getElementById('searchButton');
  const errorDiv = document.getElementById('errorDiv');

  const LengthValidation = (inputElement) => {
    const length = 2;
    const isValid = inputElement.value.length === length;

    if (isValid) {
      renderError('');
    } else {
      renderError(`Invalid input data. String must consists ${length} symbols`);
    }

    return isValid;
  };

  initSubscriptions = () => {

    searchButton.addEventListener('click', () => {
      if (LengthValidation(symbolsInput)) {
        searchSymbols();
      }
    })
  };

  searchSymbols = () => {
    const s1 = symbolsInput.value[0];
    const s2 = symbolsInput.value[1];
    const regExp = new RegExp(`[${s1}|${s2}][${s1}|${s2}]+`, 'ig');

    renderResult(strInput.value.match(regExp));
  };

  renderError = (err) => {
    errorDiv.innerHTML = err;
  };

  renderResult = (res) => {
    if (!res) {
      resultSpan.innerHTML = '';
    } else {
      resultSpan.innerHTML =
        res.reduce((acc, value) => {
          return `${acc}<div>${value} - length: ${value.length}</div>`
        }, '');
    }
  };


  this.initComponent = () => {
    initSubscriptions();
  };
}