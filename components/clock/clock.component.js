function ClockComponent() {
  // Available dom elements
  const clockCanvas = document.getElementById("clockCanvas");
  const canvasHeigth = 400;
  const canvasWidth = 400;

  const ctx = clockCanvas.getContext('2d');

  let intervalId;
  let lastRenderedTime;
  const radius = 20;
  const center = {
    x: 100,
    y: 30,
  };

  const fullCircleAngle = 360;

  this.initComponent = () => {
    intervalId = setInterval(() => {
      const currDate = new Date();
      const currTime = {
        minutes: currDate.getMinutes(),
        hours: currDate.getHours(),
        seconds: currDate.getSeconds(),
      };

      if (!lastRenderedTime || (lastRenderedTime.minutes !== currTime.minutes)) {
        renderClock(getClockParams(currTime));
        lastRenderedTime = currTime;
      }
    }, 1000);
  };


  getAngleForMinutesHand = (currTime) => {
    const angleDegree = fullCircleAngle * (currTime.minutes / 60) + (currTime.seconds / 60) * 6 - 90;

    // return result in radians
    return Math.PI / 180 * angleDegree;
  };


  getAngleForHoursHand = (currTime) => {
    const angleDegree = fullCircleAngle * (currTime.hours / 12) + (currTime.minutes / 60) * 30 - 90;

    // return result in radians
    return Math.PI / 180 * angleDegree;
  };

  /**
   * currTime: {
   * minutes: number,
   * hours: number
   * }
   * */
  getClockParams = (currTime) => {
    return {
      minuteHand: {
        x: center.x + radius * Math.cos(getAngleForMinutesHand(currTime)),
        y: center.y + radius * Math.sin(getAngleForMinutesHand(currTime)),
      },

      hourHand: {
        x: center.x + radius * Math.cos(getAngleForHoursHand(currTime)),
        y: center.y + radius * Math.sin(getAngleForHoursHand(currTime)),
      }
    }
  };

  /**
   * params: {
   * minuteHand: {
   * x: number,
   * y: number
   * }
   * hourHand: {
   * x: number,
   * y: number
   * }
   * }
   * */
  renderClock = (params) => {
    ctx.clearRect(0, 0, canvasWidth, canvasHeigth);

    // Render minute hand
    ctx.beginPath();
    ctx.strokeStyle = 'blue';
    ctx.lineWidth = 2;
    ctx.moveTo(center.x, center.y);
    ctx.lineTo(params.minuteHand.x, params.minuteHand.y);
    ctx.stroke();

    ctx.beginPath();
    ctx.strokeStyle = 'green';
    ctx.moveTo(center.x, center.y);
    ctx.lineTo(params.hourHand.x, params.hourHand.y);
    ctx.stroke();

  }
}

