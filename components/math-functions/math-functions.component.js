function MathFunctionsComponent() {
  // Available dom elements
  const numberInput = document.getElementById('numberInput');
  const sqrButton = document.getElementById('sqrButton');
  const sqrtButton = document.getElementById('sqrtButton');
  const lnButton = document.getElementById('lnButton');
  const resultSpan = document.getElementById('resultSpan');
  const errDiv = document.getElementById('errDiv');

  const inputValidation = (inputValue) => {
    const onlyDigits = (v) => {
      return v.search(/^(0|[1-9][0-9]*)$/) !== -1;
    };

    const validations = [
      onlyDigits
    ];

    const isValid = validations.every((validationFunc) =>
      validationFunc(inputValue));

    if (isValid) {
      renderError('');
    } else {
      renderError('Invalid input data. Support only digits');
    }

    return isValid;
  };

  initSubscriptions = () => {
    sqrButton.addEventListener('click', () => {
      const power = prompt('Input power');
      if (inputValidation(numberInput.value) && inputValidation(power)) {
        renderResult(Math.pow(numberInput.value, power));
      }
    });

    sqrtButton.addEventListener('click', () => {
      if (inputValidation(numberInput.value)) {
        renderResult(Math.sqrt(numberInput.value));
      }
    });

    lnButton.addEventListener('click', () => {
      if (inputValidation(numberInput.value)) {
        renderResult(Math.log(numberInput.value));
      }
    })

  };

  renderError = (err) => {
    errDiv.innerHTML = err;
  };

  renderResult = (res) => {
    resultSpan.innerHTML = res;
  };

  this.initComponent = () => {
    initSubscriptions();
  }
}